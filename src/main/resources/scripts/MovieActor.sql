create table MovieActor (
	movieId int not null,
    actorId int not null,
    constraint fkMovieActorMovieId foreign key(movieId) references Movie(id),
    constraint fkMovieActorActorId foreign key(actorId) references Person(id),
    primary key(movieId, actorId)
);
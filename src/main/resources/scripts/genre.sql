create table Genre (
	id int auto_increment,
    name varchar(30),
    primary key(id)
);

insert into Genre(name) values('action');
insert into Genre(name) values('animation');
insert into Genre(name) values('comedy');
insert into Genre(name) values('horror');
insert into Genre(name) values('drama');
create database SoftEngProjectDatabase;
use SoftEngProjectDatabase;

create table Movie (
	id int auto_increment,
	title varchar(100),
	director varchar(100),
	year int,
	movieUri varchar(500),
	imgUri varchar(500),
  genre varchar(100),
	primary key(id)
);

insert into Movie
  (title, director, year, movieUri, imgUri, genre)
values
  ('The Shawshank Redemption', 'Frank Darabont', 1994, 'https://www.youtube.com/embed/_-TxWzhc1g4', 'http://1.fwcdn.pl/po/10/48/1048/6925401.3.jpg', 'ACTION'),
  ('The Godfather', 'Francis Ford Coppola', 1972, 'https://www.youtube.com/embed/i96VS_z8y7g', 'http://1.fwcdn.pl/po/10/48/1048/6925401.3.jpg', 'ACTION'),
  ('The Dark Knight', 'Christopher Nolan', 2008, 'https://www.youtube.com/embed/_-TxWzhc1g4', 'http://1.fwcdn.pl/po/10/48/1048/6925401.3.jpg', 'ACTION');
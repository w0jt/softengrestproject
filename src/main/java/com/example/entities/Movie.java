package com.example.entities;

import com.example.repositories.MovieRepository;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "movie", uniqueConstraints = { @UniqueConstraint(columnNames = {"title", "director"}) })
@Indexed
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotEmpty
    @Field
    private String title;

    @NotEmpty
    @Field
    private String description;

    @NotNull
    @Field
    private Integer year;

    @NotEmpty
    @Field
    private String genres;

    @NotEmpty
    @Field
    private String director;

    @NotEmpty
    @Field
    private String writers;

    @NotEmpty
    @Field
    private String actors;

    @NotEmpty
    private String imgUri;

    @NotEmpty
    private String trailerUri;

    @NotEmpty
    private String fullVersionUri;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriters() {
        return writers;
    }

    public void setWriters(String writers) {
        this.writers = writers;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getImgUri() {
        return imgUri;
    }

    public void setImgUri(String imgUri) {
        this.imgUri = imgUri;
    }

    public String getTrailerUri() {
        return trailerUri;
    }

    public void setTrailerUri(String trailerUri) {
        this.trailerUri = trailerUri;
    }

    public String getFullVersionUri() {
        return fullVersionUri;
    }

    public void setFullVersionUri(String fullVersionUri) {
        this.fullVersionUri = fullVersionUri;
    }
}

package com.example.validation;

/**
 * Thrown as a result of attempting to fetch a non-existent resource
 */
public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }
}

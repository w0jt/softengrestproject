package com.example.config;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.orm.jpa.EntityManagerProxy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class FullTextIndexBuilder implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private EntityManagerProxy entityManagerProxy;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        try {
            FullTextEntityManager manager = FullTextSearchConfiguration.getFullTextEntityManager(entityManagerProxy);
            manager.createIndexer().startAndWait();
        }
        catch (InterruptedException ex) {
            System.err.println("An error occurred while building the search index");
        }
    }
}

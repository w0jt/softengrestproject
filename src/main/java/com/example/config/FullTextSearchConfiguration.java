package com.example.config;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.beans.FatalBeanException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.EntityManagerProxy;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Configuration
public class FullTextSearchConfiguration {

    @PersistenceContext
    private EntityManager entityManager;

    @Bean
    public EntityManagerProxy getEntityManagerProxy() {
        if (!(entityManager instanceof EntityManagerProxy)) {
            throw new FatalBeanException("Entity manager " + entityManager + " is not a proxy");
        }
        EntityManagerProxy entityManagerProxy = (EntityManagerProxy) entityManager;
        return entityManagerProxy;
    }

    public static FullTextEntityManager getFullTextEntityManager(EntityManagerProxy manager) {
        return Search.getFullTextEntityManager(manager.getTargetEntityManager());
    }
}

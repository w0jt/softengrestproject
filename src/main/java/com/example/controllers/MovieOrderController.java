package com.example.controllers;

import com.example.entities.Client;
import com.example.entities.Movie;
import com.example.entities.MovieOrder;
import com.example.exceptions.NoSuchEntityException;
import com.example.repositories.ClientRepository;
import com.example.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/orders")
public class MovieOrderController extends AbstractCRUDController<MovieOrder, Integer> {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private MovieRepository movieRepository;

    @Override
    @RequestMapping("")
    public Page<MovieOrder> getAll(Pageable pageable) {
        return super.getAll(pageable);
    }

    @Override
    @RequestMapping("/{id}")
    public MovieOrder get(@PathVariable Integer id) {
        return super.get(id);
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public void add(@Valid @RequestBody MovieOrder entity) {
        fetchDependenciesForEntity(entity);
        super.add(entity);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@Valid @RequestBody MovieOrder entity, @PathVariable Integer id) {
        fetchDependenciesForEntity(entity);
        super.update(entity, id);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id) {
        super.delete(id);
    }

    @Override
    protected void checkIfExists(Integer id) {
        super.checkIfExists(id);
    }

    @Override
    protected void updateEntity(MovieOrder withOldValues, MovieOrder withNewValues) {
        withOldValues.setClient(withNewValues.getClient());
        withOldValues.setMovie(withNewValues.getMovie());
        withOldValues.setDate(withNewValues.getDate());
        withOldValues.setStatus(withNewValues.getStatus());
    }

    private void fetchDependenciesForEntity(MovieOrder entity) {
        boolean clientExists = clientRepository.exists(entity.getClient().getId());
        if (!clientExists)
            throw new NoSuchEntityException("The requested Client (id = " + entity.getClient().getId() + ") could not be found");
        boolean movieExists = movieRepository.exists(entity.getMovie().getId());
        if (!movieExists)
            throw new NoSuchEntityException("The requested Movie (id = " + entity.getMovie().getId() + ") could not be found");

        Client client = clientRepository.findOne(entity.getClient().getId());
        entity.setClient(client);
        Movie movie = movieRepository.findOne(entity.getMovie().getId());
        entity.setMovie(movie);
    }
}

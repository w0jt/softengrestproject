package com.example.controllers;

import com.example.converters.SearchInfo;
import com.example.entities.Movie;
import com.example.repositories.MovieRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/movies")
public class MovieController extends AbstractCRUDController<Movie, Integer> {

    @RequestMapping("")
    public Page<Movie> getAll(@RequestParam(value = "search", required = false) SearchInfo searchInfo, Pageable pageable) {
        if (searchInfo != null) {
            MovieRepository movieRepository = (MovieRepository) repository;
            return movieRepository.search(searchInfo.getSearchQuery(), pageable);
        }
        return super.getAll(pageable);
    }

    @Override
    @RequestMapping("/{id}")
    public Movie get(@PathVariable Integer id) {
        return super.get(id);
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public void add(@Valid @RequestBody Movie entity) {
        super.add(entity);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@Valid @RequestBody Movie entity, @PathVariable Integer id) {
        super.update(entity, id);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id) {
        super.delete(id);
    }

    @Override
    protected void checkIfExists(Integer id) {
        super.checkIfExists(id);
    }

    @Override
    protected void checkIfUnique(Movie entity) {
        MovieRepository movieRepository = (MovieRepository) repository;
        List<Movie> duplicates = movieRepository.findByTitleAndDirector(entity.getTitle(), entity.getDirector());
        if (!duplicates.isEmpty()) {
            throw new DataIntegrityViolationException(  "Such a Movie (title = " + entity.getTitle() +
                                                        ", director = " + entity.getDirector() + ") already exists");
        }
    }

    @Override
    protected void updateEntity(Movie withOldValues, Movie withNewValues) {
        //id remains the same - no need to update it

        withOldValues.setTitle(withNewValues.getTitle());
        withOldValues.setActors(withNewValues.getActors());
        withOldValues.setDescription(withNewValues.getDescription());
        withOldValues.setDirector(withNewValues.getDirector());
        withOldValues.setFullVersionUri(withNewValues.getFullVersionUri());
        withOldValues.setGenres(withNewValues.getGenres());
        withOldValues.setTrailerUri(withNewValues.getTrailerUri());
        withOldValues.setWriters(withNewValues.getWriters());
        withOldValues.setYear(withNewValues.getYear());
        withOldValues.setImgUri(withNewValues.getImgUri());
    }

}

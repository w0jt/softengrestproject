package com.example.controllers;


import com.example.validation.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;

/**
 * Generic abstract controller for handling CRUD-like requests
 */
public abstract class AbstractCRUDController<T, ID extends Serializable> {

    @Autowired
    protected PagingAndSortingRepository<T, ID> repository;

    public Page<T> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public T get(ID id) {
        checkIfExists(id);
        return repository.findOne(id);
    }

    public void add(T entity) {
        checkIfUnique(entity);
        repository.save(entity);
    }

    public void update(T entity, ID id){
        checkIfExists(id);
        checkIfUnique(entity);
        T oldEntity = repository.findOne(id);
        updateEntity(oldEntity, entity);
        repository.save(oldEntity);
    }

    public void delete(ID id) {
        checkIfExists(id);
        repository.delete(id);
    }

    protected void checkIfExists(ID id) {
        if (!repository.exists(id))
            throw new ResourceNotFoundException("The requested entity could not be found");
    }

    protected void checkIfUnique(T entity) {
    }

    protected abstract void updateEntity(T withOldValues, T withNewValues);

}

package com.example.controllers;

import com.example.entities.Client;
import com.example.repositories.ClientRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.util.List;

@RestController
@RequestMapping("/api/clients")
public class ClientController extends AbstractCRUDController<Client, Integer> {

    @Override
    @RequestMapping("")
    public Page<Client> getAll(Pageable pageable) {
        return super.getAll(pageable);
    }

    @Override
    @RequestMapping("/{id}")
    public Client get(@PathVariable Integer id) {
        return super.get(id);
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public void add(@Valid @RequestBody Client entity) {
        super.add(entity);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@Valid @RequestBody Client entity, @PathVariable Integer id) {
        super.update(entity, id);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id) {
        super.delete(id);
    }

    @Override
    protected void checkIfExists(Integer id) {
        super.checkIfExists(id);
    }

    @Override
    protected void checkIfUnique(Client entity) {
        ClientRepository clientRepository = (ClientRepository) repository;
        Client duplicate = clientRepository.findByFirstnameAndLastname(entity.getFirstname(), entity.getLastname());
        if (duplicate != null) {
            throw new DataIntegrityViolationException(  "Such a Client (firstname = " + entity.getFirstname() +
                                                        ", lastname = " + entity.getLastname() + ") already exists");
        }

        duplicate = clientRepository.findByLogin(entity.getLogin());
        if (duplicate != null) {
            throw new DataIntegrityViolationException(  "Such a Client (login = " + entity.getLogin() + ") already exists");
        }
    }

    @Override
    protected void updateEntity(Client withOldValues, Client withNewValues) {
        //id remains the same - no need to update it

        withOldValues.setFirstname(withNewValues.getFirstname());
        withOldValues.setLastname(withNewValues.getLastname());
        withOldValues.setLogin(withNewValues.getLogin());
        withOldValues.setPassword(withNewValues.getPassword());
    }
}

package com.example.repositories;

import com.example.entities.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MovieRepository extends PagingAndSortingRepository<Movie, Integer>, FullTextSearchableRepository<Movie> {

    List<Movie> findByTitleAndDirector(String title, String director);
}

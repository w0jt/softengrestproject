package com.example.repositories;

import com.example.config.FullTextSearchConfiguration;
import com.example.entities.Movie;
import org.apache.lucene.search.Query;
import org.hibernate.search.errors.EmptyQueryException;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.orm.jpa.EntityManagerProxy;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class MovieRepositoryImpl implements FullTextSearchableRepository<Movie> {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private EntityManagerProxy entityManagerProxy;

    @Override
    public Page<Movie> search(String query, Pageable pageable) {
        FullTextEntityManager manager = FullTextSearchConfiguration.getFullTextEntityManager(entityManagerProxy);

        QueryBuilder fullTextSearchBuilder = manager.getSearchFactory().buildQueryBuilder().forEntity(Movie.class).get();
        Query lucene =  null;

        try {
            lucene = fullTextSearchBuilder.keyword()
                    .onFields("title", "description", "year", "genres", "director", "writers", "actors")
                    .matching(query)
                    .createQuery();
        }
        catch (EmptyQueryException ex) {
            return new PageImpl<Movie>(new ArrayList<>(), pageable, 0L);
        }

        FullTextQuery q = manager.createFullTextQuery(lucene, Movie.class);
        q.setProjection("id");
        q.setFirstResult(pageable.getOffset()).setMaxResults(pageable.getPageSize());

        long resultSize = q.getResultSize();
        List<Object[]> resultList = q.getResultList();
        List<Integer> foundIds = new ArrayList<>();
        for (Object[] result : resultList) {
            Integer movieId = (Integer) result[0];
            foundIds.add(movieId);
        }

        CriteriaBuilder sortBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Movie> sortQuery = sortBuilder.createQuery(Movie.class);
        Root<Movie> root = sortQuery.from(Movie.class);

        Expression<Integer> idExpression = root.get("id");
        Predicate inFoundIdsPredicate = idExpression.in(foundIds);
        sortQuery.where(inFoundIdsPredicate);
        sortQuery.orderBy(QueryUtils.toOrders(pageable.getSort(), root, sortBuilder));
        List<Movie> foundMovies = entityManager.createQuery(sortQuery.select(root)).getResultList();

        return new PageImpl<Movie>(foundMovies, pageable, resultSize);
    }
}

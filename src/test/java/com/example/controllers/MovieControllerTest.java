package com.example.controllers;

import com.example.TestRoot;
import com.example.entities.Movie;
import com.example.exceptions.NoSuchEntityException;
import com.example.repositories.MovieRepository;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;


import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MovieControllerTest extends TestRoot {

    @Mock
    private MovieRepository movieRepositoryMock;

    @Autowired
    @InjectMocks
    private MovieController movieController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(movieRepositoryMock);
        mockMvc = MockMvcBuilders.standaloneSetup(movieController).build();

        when(movieRepositoryMock.findOne(1)).thenReturn(getNewTmpMovie(1, "SuperTitle"));
        when(movieRepositoryMock.exists(1)).thenReturn(true);
        when(movieRepositoryMock.exists(2)).thenReturn(false);

        doNothing().when(movieRepositoryMock).delete(1);
        doThrow(new NoSuchEntityException("Test")).doNothing().when(movieRepositoryMock).delete(2);
    }

    @Test
    public void getMovieBadRequest() throws Exception {
        mockMvc
                .perform(get("/api/movies/string"))
                .andExpect(status().isBadRequest());
        verifyZeroInteractions(movieRepositoryMock);
    }

    @Test(expected = NestedServletException.class)
    public void getMovieByIdInvalid() throws Exception {
        mockMvc
                .perform(get("/api/movies/2"))
                .andExpect(status().isNotFound());
        verify(movieRepositoryMock, times(1)).exists(2);
        verifyNoMoreInteractions(movieRepositoryMock);
    }

    @Test
    public void getMovieByIDValid() throws Exception {
        mockMvc
                .perform(get("/api/movies/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("SuperTitle")));
        verify(movieRepositoryMock, times(1)).exists(1);
        verify(movieRepositoryMock, times(1)).findOne(1);
        verifyNoMoreInteractions(movieRepositoryMock);
    }

    @Test
    public void addMovieInvalidRequest() throws Exception {
        mockMvc
                .perform(post("/api/movies/adwdaw"))
                .andExpect(status().isBadRequest());
        verifyZeroInteractions(movieRepositoryMock);
    }

    @Test
    public void addMovieNotSupportedType() throws Exception {
        mockMvc
                .perform(post("/api/movies/")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .sessionAttr("movie", new Movie()))
                .andExpect(status().isUnsupportedMediaType());
        verifyZeroInteractions(movieRepositoryMock);
    }

    @Test
    public void addMovieValid() throws Exception {
        Gson gson = new Gson();
        Movie newMovie = getNewTmpMovie(2, "FilmAdd");
        String json = gson.toJson(newMovie);

        when(movieRepositoryMock.findByTitleAndDirector("FilmAdd", "Test Director")).thenReturn(Arrays.asList());

        mockMvc
                .perform(post("/api/movies/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
        verify(movieRepositoryMock, times(1)).save(any(Movie.class));
    }

    @Test(expected = NestedServletException.class)
    public void addMovieDuplicated() throws Exception {
        Gson gson = new Gson();
        Movie newMovie = getNewTmpMovie(1, "SuperTitle");
        String json = gson.toJson(newMovie);

        when(movieRepositoryMock.findByTitleAndDirector("SuperTitle", "Test Director")).thenReturn(Arrays.asList(newMovie));

        mockMvc
                .perform(post("/api/movies/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
        verify(movieRepositoryMock, times(1)).save(any(Movie.class));
    }

    @Test
    public void deleteMovieInvalidRequest() throws Exception {
        mockMvc
                .perform(delete("/api/movies/adwwd"))
                .andExpect(status().isBadRequest());
        verifyZeroInteractions(movieRepositoryMock);
    }

    @Test
    public void deleteMovieByIdValid() throws Exception {
        mockMvc
                .perform(delete("/api/movies/1"))
                .andExpect(status().isOk());
        verify(movieRepositoryMock, times(1)).exists(1);
        verify(movieRepositoryMock, times(1)).delete(1);
        verifyNoMoreInteractions(movieRepositoryMock);
    }

    @Test( expected = NestedServletException.class)
    public void deleteMovieByIdInvalid() throws Exception {
        mockMvc
                .perform(delete("/api/movies/2"))
                .andExpect(status().isNotFound());
        verifyZeroInteractions(movieRepositoryMock);
    }

    public static Movie getNewTmpMovie(int id, String title) {
        Movie movie = new Movie();
        movie.setId(id);
        movie.setTitle(title);

        movie.setYear(1999);
        movie.setDirector("Test Director");
        movie.setImgUri("img/uri");
        movie.setActors("Test Actor");
        movie.setDescription("Description");
        movie.setFullVersionUri("img/fullUri");
        movie.setGenres("Horror");
        movie.setTrailerUri("trailer/uri");
        movie.setWriters("Writers");
        return movie;
    }

}
package com.example.controllers;

import com.example.SoftEngProjectApplication;
import com.example.TestRoot;
import com.example.entities.MovieOrder;
import com.example.exceptions.NoSuchEntityException;
import com.example.repositories.MovieOrderRepository;
import com.example.validation.ResourceNotFoundException;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;


import java.sql.Time;
import java.time.LocalTime;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MovieOrderControllerTest extends TestRoot {
    @Mock
    private MovieOrderRepository repoMock;

    @Autowired
    @InjectMocks
    private MovieOrderController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(repoMock);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();




        when(repoMock.findOne(1)).thenReturn(getNewMovieOrder(1, MovieOrder.OrderStatus.UNFULFILLED));
        when(repoMock.exists(1)).thenReturn(true);
        when(repoMock.exists(2)).thenReturn(false);
    }

    @Test
    public void getOrderValid() throws Exception {
        mockMvc
                .perform(get("/api/orders/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.status", is("UNFULFILLED")))
                .andExpect(jsonPath("$.date").exists())
                .andExpect(jsonPath("$.client").exists())
                .andExpect(jsonPath("$.movie").exists());
        verify(repoMock).exists(1);
        verify(repoMock).findOne(1);
        verifyNoMoreInteractions(repoMock);
    }

    @Test(expected = NestedServletException.class)
    public void getOrderInvalid() throws Exception {
        mockMvc
                .perform(get("/api/orders/2"))
                .andExpect(status().isNotFound());
    }

    public static MovieOrder getNewMovieOrder(int id, MovieOrder.OrderStatus status) {
        MovieOrder order = new MovieOrder();
        order.setId(id);
        order.setStatus(status);
        order.setDate(Time.valueOf(LocalTime.now()));
        order.setClient(ClientControllerTest.getNewTmpClient(1, "name", "lastname", "login"));
        order.setMovie(MovieControllerTest.getNewTmpMovie(1, "title"));
        return  order;
    }
}